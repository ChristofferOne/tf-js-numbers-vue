// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'

import VueFusionCharts from 'vue-fusioncharts';
import FusionCharts from 'fusioncharts';
import Charts from 'fusioncharts/fusioncharts.charts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion'

Vue.config.productionTip = false

Vue.use(VueFusionCharts, FusionCharts, Charts, FusionTheme)

Vue.prototype.$http = axios;

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
